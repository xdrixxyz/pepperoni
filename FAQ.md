## FAQ

### What is Pepperoni?
Pepperoni is the codename for my v6 website rewrite.

### Why Pepperoni?
Because pepperoni is better than pineapples on pizza.

### What happened to the old `hexexpeck.github.io` repository?
I have made the executive decision to delete it, because it got way too cluttered and disorganized. I've created this one to replace it.
I also changed my name to xDrixxyz so yeah...

### Bu-but you lost your 1,000 commits!
No worries, we'll get there again! #Pepperoni1kCommits

### Does your site (`xdrixxyz.ml`) redirect to this new rewrite?
Yes, effective May 11, 2018 my website will instantly redirect and cache the new rewrite as opposed to the old version.

### Why didn't you back up the old version?
React > Writing HTML by hand

### Does this count as a whole new version?
Yes because I wrote it in mostly JavaScript using React. Welcome v6.

### When will v7 come out?
When I feel like it's time for a v7.

### How many versions will there be?
idk
