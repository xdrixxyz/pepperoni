# pepperoni
let's start fresh

## How to run locally

> Make sure you set the `PUBLIC_URL` environment variable, it is needed to determine where to save the service worker script.

**Prerequisites**:
- Yarn or NPM
- Node.js
- Git

Run these commands:
```
$ git clone https://gitlab.com/xdrixxyz/pepperoni
$ yarn install
$ yarn start
```
> Note that if you are using NPM, replace `yarn install` with `npm install` and replace `yarn start` with `npm run start`.

After that, you will get a development server running on `localhost:3000`, next open up your favorite browser and visit http://localhost:3000 and enjoy!

Any changes you make to the source code while the development server is running will automatically reload the development webpage in your browser.

## Building from source

> Make sure you set the `PUBLIC_URL` environment variable, it is needed to determine where to save the service worker script.

**Prerequisites**:
- Yarn or NPM
- Node.js
- Git

Run these commands:
```
$ git clone https://gitlab.com/xdrixxyz/pepperoni
$ yarn install
$ yarn build
```
> Note that if you are using NPM, replace `yarn install` with `npm install` and replace `yarn build` with `npm run build`.

After that, you will get a new directory created called `build`. In it, you can find the built files that you can statically host using a service such as [GitLab Pages](https://about.gitlab.com/features/pages/) - just **don't try to blankly host a copy of my website, you'll get taken down faster than you can say pepperoni**

_Wanna see the FAQ? Yeah, the (very) **F**requently **A**sked **Q**uestions! [Click on me!](https://gitlab.com/xdrixxyz/pepperoni/blob/master/FAQ.md)_
