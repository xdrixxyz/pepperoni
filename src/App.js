import React, { Component } from 'react';
import './App.css';
import { Accordion, AccordionItem } from 'react-sanfona';

class Pepperoni extends Component {
  render() {
    return(
      <div className="Pepperoni">
        <header>
          <h1 className="Pepperoni-header">xDrixxyz</h1>
          <p className="Pepperoni-text">I make 0's and 1's.</p>
        </header>
        <br></br>
        <Accordion className="Pepperoni-text">
          {["▾ Links"].map(item => {
            return (
              <AccordionItem className="Pepperoni-text" key={item} title={item} expanded={item === 1}>
                <div>
                  <p className="Pepperoni-text"><i className="fab fa-gitlab"></i> GitLab: <a href="https://gitlab.com/xdrixxyz">xDrixxyz</a></p>
                  <p className="Pepperoni-text"><i className="fab fa-keybase"></i> Keybase: <a href="https://keybase.io/xdrixxyz">xdrixxyz</a></p>
                  <p className="Pepperoni-text"><i className="fab fa-twitter"></i> Twitter: <a href="https://twitter.com/xDrixxyz">xDrixxyz</a></p>
                  <p className="Pepperoni-text"><i className="fab fa-reddit-alien"></i> Reddit: <a href="https://reddit.com/u/Hexexpeck">/u/Hexexpeck</a></p>
                  <p className="Pepperoni-text"><i className="fab fa-discord"></i> Discord: <strong>xDrixxyz#0001</strong></p>
                  <p className="Pepperoni-text"><i className="fab fa-blogger-b"></i> Blog: <a href="https://blog.xdrixxyz.ml">xDrixxyz's Cozy Home</a></p>
                  <p className="Pepperoni-text"><i className="fas fa-at"></i> Email: <a href="mailto:xd@xdrixxyz.ml">xd[at]xdrixxyz[dot]ml</a></p>
				  <p className="Pepperoni-text"><i className="fab fa-bitcoin"></i> Bitcoin: <a href="bitcoin:33E3SaPVuKXJYuDBJFS78avML7oiDHWus4">33E3SaPVuKXJYuDBJFS78avML7oiDHWus4</a></p>
                </div>
              </AccordionItem>
            )
          })}
        </Accordion>
        <Accordion className="Pepperoni-text">
          {["▾ Tools"].map(item2 => {
            return (
              <AccordionItem className="Pepperoni-text" key={item2} title={item2} expanded={item2 === 1}>
                <div>
                  <p className="Pepperoni-text"><i className="fab fa-nintendo-switch"></i> <a href="/modmyswitch">ModMySwitch</a></p>
                </div>
              </AccordionItem>
            )
          })}
        </Accordion>
        <br></br>
        <footer>
          <p className="Pepperoni-text">You can view this site's source code <a href="https://gitlab.com/xdrixxyz/pepperoni">on GitLab</a>!</p>
          <br></br>
          <p className="Pepperoni-text-secret">Protip: Click the arrow next to the option you want above to expand the dropdown!</p>
		  <br></br>
		  <p className="Pepperoni-text-secret">Build {new Date().toISOString()}</p>
        </footer>
      </div>
    );
  }
}

export default Pepperoni;