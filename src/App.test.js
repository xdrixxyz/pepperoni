import React from 'react';
import ReactDOM from 'react-dom';
import Pepperoni from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Pepperoni />, div);
  ReactDOM.unmountComponentAtNode(div);
});
